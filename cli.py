import os
import click
import logging

from prettytable import PrettyTable

from astrohat.client import AstrohatClient
from astrohat.models import ChannelType

ASTROHAT_HOST = os.environ.get("ASTROHAT_HOST", "astrohat.sock")
create_client = lambda: AstrohatClient(ASTROHAT_HOST)


@click.group()
@click.option("--debug", is_flag=True, help="Run in debug mode.")
def cli(debug=False):
    """
    Smart power for all your astronomy equipment.
    """

    logging.basicConfig(
        level=logging.DEBUG if debug else logging.INFO,
        format="%(asctime)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"
    )


@cli.group()
def channel():
    """
    Control and retrieve channels.
    """


@channel.command()
@click.argument("id", type=click.STRING)
@click.argument("enabled", type=click.BOOL)
def set(id, enabled):
    """
    Set the on/off state of a channel.
    ID is the channel's identifier.
    ENABLED is whether the channel is ENABLED/DISABLED
    """

    with create_client() as client:
        success, error = client.set_channel(id, enabled)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.argument("id", type=click.STRING)
def toggle(id):
    """
    Toggle a channel on or off.
    ID is the channel's identifier.
    """

    with create_client() as client:
        success, error = client.toggle_channel(id)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.argument("id", type=click.STRING)
def enable(id):
    """
    Enable a channel.
    ID is the channel's identifier.
    """

    with create_client() as client:
        success, error = client.set_channel(id, True)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.argument("id", type=click.STRING)
def disable(id):
    """
    Disable a channel.
    ID is the channel's identifier.
    """

    with create_client() as client:
        success, error = client.set_channel(id, False)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.argument("id", type=click.STRING)
@click.argument("name", type=click.STRING)
def name(id, name):
    """
    Update the name of a channel.
    ID is the channel's identifier.
    NAME is the new name of the channel.
    """

    with create_client() as client:
        success, error = client.channel_name(id, name)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.argument("id", type=click.STRING)
@click.argument("value", type=click.FLOAT)
def max_ampere(id, value):
    """
    Set the maximum ampere a channel can consume.
    ID is the channel's identifier.
    VALUE is the maximum ampere this channel can consume.
    """

    with create_client() as client:
        success, error = client.channel_max_ampere(id, value)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.argument("id", type=click.STRING)
@click.argument("power", type=click.INT)
def power(id, power):
    """
    Update the power setting of a heater channel.
    ID is the channel's identifier.
    POWER is the power percentage of the heater channel.
    """

    with create_client() as client:
        success, error = client.heater_power(id, power)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.argument("id", type=click.STRING)
@click.argument("voltage", type=click.STRING)
def voltage(id, voltage):
    """
    Change the output voltage of a channel.
    ID is the channel's identifier.
    VOLTAGE is the voltage to output from a channel.
    """

    with create_client() as client:
        success, error = client.channel_voltage(id, voltage)

        if not success:
            click.echo(error, err=True)


@channel.command()
@click.option("--id", required=False, type=str)
@click.option("--type", required=False, type=str)
@click.option("--sortby", default="Id", type=str)
def status(id, type, sortby):
    """
    Get the status of one or more channels.
    ID is the channel's identifier.
    TYPE is the channel's type.
    SORTBY is the column to sort the channel status by.
    """
    def create_channel_row(channel_status):
        NA = "-"

        row_builders = {
            ChannelType.HEATER:
            lambda d: [
                d.id, d.type, d.name, d.voltage,
                bool(d.enabled), d.ampere, d.max_ampere, f"{d.power}%"
            ],
            ChannelType.POWER:
            lambda d: [
                d.id, d.type, d.name, d.voltage,
                bool(d.enabled), d.ampere, d.max_ampere, NA
            ],
            ChannelType.ADJ:
            lambda d:
            [d.id, d.type, d.name, d.voltage,
             bool(d.enabled), NA, NA, NA]
        }

        builder = row_builders[channel_status.type]

        return builder(channel_status)

    with create_client() as client:
        success, channel_status = client.channel_status(id, type)

        if not success:
            click.echo("Error while retrieving channel status", err=True)

            return

    table = PrettyTable()

    table.field_names = [
        "Id", "Type", "Name", "Voltage", "Enabled", "Ampere", "Max Ampere",
        "Power"
    ]

    table.add_rows(create_channel_row(c) for c in channel_status)

    click.echo(table.get_string(sortby=sortby))


if __name__ == "__main__":
    cli()
