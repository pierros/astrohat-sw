import os
import logging
import queue
import socket

from threading import Thread

from socketserver import UnixStreamServer, StreamRequestHandler

from tinyrpc.transports import ServerTransport, ClientTransport

BUFFER_SIZE = 4096


class UnixRequestHandler(StreamRequestHandler):
    def __init__(self, message_queue):
        self._message_queue = message_queue

    def __call__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def handle(self):
        logging.debug("Client connected")

        # Create a new reply queue for each client.
        reply_queue = queue.Queue()

        connected = True

        while connected:
            chunks = []

            try:
                while True:
                    data = self.request.recv(BUFFER_SIZE)

                    if not data:
                        connected = False

                        break

                    chunks.append(data)

                    # If there is no more pending data then break.
                    if len(data) < BUFFER_SIZE:
                        break
            except (socket.timeout, socket.error):
                connected = False

                break

            if not connected:
                break

            if not chunks:
                continue

            message = b''.join(chunks)
            self._message_queue.put((reply_queue, message))

            reply = reply_queue.get()

            self.request.send(reply)

        self.request.close()
        logging.debug("Client disconnected")


class UnixStreamServerTransport(ServerTransport):
    def __init__(self, path):
        self._path = path

        self._message_queue = queue.Queue()

        dir_name = os.path.dirname(path)

        if os.path.exists(path):
            os.remove(path)
        elif dir_name and not os.path.exists(dir_name):
            os.makedirs(dir_name)

        self._server = UnixStreamServer(
            path, UnixRequestHandler(self._message_queue), True
        )

        server_thread = Thread(target=self._server.serve_forever)

        server_thread.setDaemon(True)
        server_thread.start()

    def receive_message(self):
        return self._message_queue.get()

    def send_reply(self, context, reply):
        # The context is the reply queue for the client.
        context.put(reply)

    def close(self):
        self._server.server_close()

        os.remove(self._path)


class UnixStreamClientTransport(ClientTransport):
    def __init__(self, path):
        self._sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self._sock.connect(path)

    def send_message(self, message, expect_reply=True):
        self._sock.send(message)

        if not expect_reply:
            return None

        chunks = []

        while True:
            try:
                data = self._sock.recv(BUFFER_SIZE)
            except socket.timeout:
                break

            if not data:
                break

            chunks.append(data)

            # If there is no more pending data then break.
            if len(data) < BUFFER_SIZE:
                break

        reply = b''.join(chunks)

        return reply

    def close(self):
        self._sock.close()
