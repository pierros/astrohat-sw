import logging

from threading import Thread

from tinyrpc.server import RPCServer
from tinyrpc.dispatch import RPCDispatcher
from tinyrpc.protocols.jsonrpc import JSONRPCProtocol

from .driver import driver
from .procedures import ChannelProcedures
from .transport import UnixStreamServerTransport


class AstrohatServer:
    def __init__(self, address):
        self._address = address

        self._transport = None

    def start(self):
        self._transport = UnixStreamServerTransport(self._address)

        protocol = JSONRPCProtocol()
        dispatcher = RPCDispatcher()

        # Register the available remote procedures.
        dispatcher.register_instance(ChannelProcedures())

        server = RPCServer(self._transport, protocol, dispatcher)

        # Setup debug logging for inbound/outbound messages.
        server.trace = lambda dir, ctx, msg: logging.debug(f"{dir}{msg}")

        driver_thread = Thread(target=driver.start)
        driver_thread.start()

        logging.info(f"Starting Astrohat server on '{self._address}'")

        try:
            server.serve_forever()
        except KeyboardInterrupt:
            pass

        self.stop()

    def stop(self):
        driver.stop()

        logging.info("Shutting down Astrohat server")

        self._transport.close()
