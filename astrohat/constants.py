HEADER = """
  ___      _             _           _
 / _ \    | |           | |         | |
/ /_\ \___| |_ _ __ ___ | |__   __ _| |_
|  _  / __| __| '__/ _ \| '_ \ / _` | __|
| | | \__ \ |_| | | (_) | | | | (_| | |_
\_| |_/___/\__|_|  \___/|_| |_|\__,_|\__|

"""

DRIVER_CYCLE_DELAY = 1 / 10

ADC_VOLTAGE = 5.0
ADC_SAMPLES = 3
ADC_SPI_BUS = 1
ADC_SPI_DEVICE = 0

KILLIS = 5_000
MAX_AMPERE = 3.0

DEFAULT_HEATER_POWER = 50
HEATER_FREQUENCY = 5_000
HEATER_ADC_CHANNEL = 2
HEATERS_DEN_PIN = 14
HEATERS_DSEL_PIN = 15

HEATER_1_IN_PIN = 17
HEATER_1_DSEL_STATE = False

HEATER_2_IN_PIN = 27
HEATER_2_DSEL_STATE = True

POWER_PAIR_1_ADC_CHANNEL = 0
POWER_PAIR_1_DEN_PIN = 10
POWER_PAIR_1_DSEL_PIN = 11

POWER_PAIR_2_ADC_CHANNEL = 1
POWER_PAIR_2_DEN_PIN = 12
POWER_PAIR_2_DSEL_PIN = 26

POWER_1_IN_PIN = 23
POWER_1_DSEL_STATE = True

POWER_2_IN_PIN = 22
POWER_2_DSEL_STATE = False

POWER_3_IN_PIN = 25
POWER_3_DSEL_STATE = True

POWER_4_IN_PIN = 24
POWER_4_DSEL_STATE = False

ADJ_IN_PIN = 13
