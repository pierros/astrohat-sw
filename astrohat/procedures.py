import logging

from tinyrpc.dispatch import public

from . import constants
from .driver import driver

from .settings import save_channels
from .channels import (
    ChannelType, ChannelVoltage, HeaterChannel, MonitoredChannel, PowerChannel,
    AdjustableChannel, SwitchableChannel
)

from .models import (
    ChannelResponseCode, ChannelStatus, HeaterChannelStatus,
    PowerChannelStatus, AdjustableChannelStatus
)


class ChannelProcedures:
    @staticmethod
    @public
    def set_channel(channel_id, enabled):
        logging.info(
            f"Set Channel: (channel_id={channel_id}, enabled={enabled})"
        )

        if not isinstance(enabled, bool):
            logging.warning("Invalid enabled value specified")

            return ChannelResponseCode.INVALID_VALUE

        if channel_id.lower() == "all":
            for channel in driver.channels:
                if isinstance(channel, SwitchableChannel):
                    channel.enabled = enabled
        else:
            channel = driver.channel(channel_id)
            if not channel:
                logging.warning(f"Unknown channel id: {channel_id}")

                return ChannelResponseCode.UNKNOWN_CHANNEL_ID

            if not isinstance(channel, SwitchableChannel):
                logging.warning(f"Unable to set channel: {channel_id}")

                return ChannelResponseCode.INVALID_CHANNEL_TYPE

            channel.enabled = enabled

        save_channels(driver.channels)

        return ChannelResponseCode.SUCCESS

    @staticmethod
    @public
    def toggle_channel(channel_id):
        logging.info(f"Toggle Channel: (channel_id={channel_id})")

        channel = driver.channel(channel_id)
        if not channel:
            logging.warning(f"Unknown channel id: {channel_id}")

            return ChannelResponseCode.UNKNOWN_CHANNEL_ID

        if not isinstance(channel, SwitchableChannel):
            logging.warning(f"Unable to toggle channel: {channel_id}")

            return ChannelResponseCode.INVALID_CHANNEL_TYPE

        channel.toggle()

        save_channels(driver.channels)

        return ChannelResponseCode.SUCCESS

    @staticmethod
    @public
    def channel_name(channel_id, channel_name):
        logging.info(
            "Channel Rename: "
            f"channel_id={channel_id}, channel_name={channel_name})"
        )

        if not isinstance(channel_name, str):
            logging.warning("Invalid channel name value specified")

            return ChannelResponseCode.INVALID_VALUE

        channel = driver.channel(channel_id)
        if not channel:
            logging.warning(f"Unknown channel id: {channel_id}")

            return ChannelResponseCode.UNKNOWN_CHANNEL_ID

        channel.name = channel_name

        save_channels(driver.channels)

        return ChannelResponseCode.SUCCESS

    @staticmethod
    @public
    def channel_max_ampere(channel_id, max_ampere):
        logging.info(
            "Channel Max Ampere: "
            f"(channel_id={channel_id}, max_ampere={max_ampere})"
        )

        if (not isinstance(max_ampere, (float, int)) or max_ampere <= 0
                or max_ampere > constants.MAX_AMPERE):
            logging.warning("Invalid max ampere specified")

            return ChannelResponseCode.INVALID_VALUE

        channel = driver.channel(channel_id)
        if not channel:
            logging.warning(f"Unknown channel id: {channel_id}")

            return ChannelResponseCode.UNKNOWN_CHANNEL_ID

        if not isinstance(channel, MonitoredChannel):
            logging.warning(f"Unable to configure channel: {channel_id}")

            return ChannelResponseCode.INVALID_CHANNEL_TYPE

        channel.max_ampere = max_ampere

        save_channels(driver.channels)

        return ChannelResponseCode.SUCCESS

    @staticmethod
    @public
    def heater_power(channel_id, power):
        logging.info(f"Heater Power: (channel_id={channel_id}, power={power})")

        if not isinstance(power, (float, int)) or power <= 0 or power > 100:
            logging.warning("Invalid heater power specified")

            return ChannelResponseCode.INVALID_VALUE

        channel = driver.channel(channel_id)
        if not channel:
            logging.warning(f"Unknown channel id: {channel_id}")

            return ChannelResponseCode.UNKNOWN_CHANNEL_ID

        if not isinstance(channel, HeaterChannel):
            logging.warning(f"Unable to configure channel: {channel_id}")

            return ChannelResponseCode.INVALID_CHANNEL_TYPE

        channel.power = int(power)

        save_channels(driver.channels)

        return ChannelResponseCode.SUCCESS

    @staticmethod
    @public
    def channel_voltage(channel_id, voltage):
        logging.info(
            f"Channel Voltage: (channel_id={channel_id}, voltage={voltage})"
        )

        if not voltage or not ChannelVoltage.has_value(voltage):
            logging.warning(f"Unknown channel voltage: {voltage}")

            return ChannelResponseCode.INVALID_VALUE

        channel = driver.channel(channel_id)
        if not channel:
            logging.warning(f"Unknown channel id: {channel_id}")

            return ChannelResponseCode.UNKNOWN_CHANNEL_ID

        if not isinstance(channel, AdjustableChannel):
            logging.warning(f"Unable to configure channel: {channel_id}")

            return ChannelResponseCode.INVALID_CHANNEL_TYPE

        channel.voltage = ChannelVoltage(voltage)

        save_channels(driver.channels)

        return ChannelResponseCode.SUCCESS

    @staticmethod
    @public
    def channel_status(channel_id=None, channel_type=None):
        logging.info(
            "Channel Status: "
            f"(channel_id={channel_id}, channel_type={channel_type})"
        )

        channels = []

        if channel_type and not ChannelType.has_value(channel_type):
            logging.warning(f"Unknown channel type: {channel_type}")

            return ChannelStatus(code=ChannelResponseCode.UNKNOWN_CHANNEL_TYPE)

        if channel_id:
            channel = driver.channel(channel_id)
            if not channel:
                logging.warning(f"Unknown channel id: {channel_id}")

                return ChannelStatus(
                    code=ChannelResponseCode.UNKNOWN_CHANNEL_ID
                )

            if not channel_type or channel.type == channel_type:
                channels.append(channel)
        else:
            channels.extend(
                filter(
                    lambda c: not channel_type or c.type == channel_type,
                    driver.channels
                )
            )

        status_builders = {
            HeaterChannel:
            lambda c: HeaterChannelStatus(
                id=c.id,
                type=c.type,
                name=c.name,
                voltage=c.voltage,
                enabled=c.enabled,
                power=c.power,
                ampere=round(c.ampere, 2),
                max_ampere=round(c.max_ampere, 2)
            ),
            PowerChannel:
            lambda c: PowerChannelStatus(
                id=c.id,
                type=c.type,
                name=c.name,
                voltage=c.voltage,
                enabled=c.enabled,
                ampere=round(c.ampere, 2),
                max_ampere=round(c.max_ampere, 2)
            ),
            AdjustableChannel:
            lambda c: AdjustableChannelStatus(
                id=c.id,
                type=c.type,
                name=c.name,
                voltage=c.voltage,
                enabled=c.enabled
            )
        }

        channel_status = [status_builders[type(c)](c) for c in channels]

        return ChannelStatus(
            code=ChannelResponseCode.SUCCESS, channels=channel_status
        )
