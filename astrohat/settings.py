import os
import json
import logging

from dataclasses import dataclass

from . import constants
from .channels import (
    AdjustableChannel, ChannelIdentifier, ChannelType, ChannelVoltage,
    PowerChannel, HeaterChannel
)

ASTROHAT_SETTINGS = os.environ.get("ASTROHAT_SETTINGS", "settings.json")


@dataclass
class ChannelDescriptor:
    in_pin: int
    adc_channel: int = 0
    den_pin: int = 0
    dsel_pin: int = 0
    dsel_state: bool = False


@dataclass
class HeaterChannelSettings:
    id: str
    type: str
    name: str
    max_ampere: float
    power: int
    enabled: bool


@dataclass
class PowerChannelSettings:
    id: str
    type: str
    name: str
    max_ampere: float
    enabled: bool


@dataclass
class AdjustableChannelSettings:
    id: str
    type: str
    name: str
    voltage: float
    enabled: bool


def save_channels(channels):
    logging.debug(f"Saving channel settings to '{ASTROHAT_SETTINGS}'")

    channel_settings = [_create_settings_from_channel(c) for c in channels]

    with open(ASTROHAT_SETTINGS, "w") as settings:
        json.dump(channel_settings, settings, indent=4, default=vars)


def load_channels():
    logging.debug(f"Loading channel settings from '{ASTROHAT_SETTINGS}'")

    with open(ASTROHAT_SETTINGS, "r") as settings:
        channel_settings = json.load(settings)

        channels = [
            _create_channel_from_settings_data(d) for d in channel_settings
        ]

        return channels


def load_or_create_channels():
    # The file already exists so we can just load any existing settings.
    if os.path.exists(ASTROHAT_SETTINGS):
        return load_channels()

    def create_default_channel(channel_id):
        descriptor = _get_channel_descriptor(channel_id)
        settings = _get_default_channel_settings(channel_id)

        return _create_channel(descriptor, settings)

    # Create a settings file with the channel defaults.
    default_channels = [create_default_channel(i) for i in ChannelIdentifier]

    logging.debug("Creating default channel settings")

    save_channels(default_channels)

    return default_channels


def _create_settings_from_channel(channel):
    settings_builders = {
        ChannelType.HEATER:
        lambda c: HeaterChannelSettings(
            id=c.id,
            type=c.type,
            name=c.name,
            max_ampere=c.max_ampere,
            power=c.power,
            enabled=c.enabled
        ),
        ChannelType.POWER:
        lambda c: PowerChannelSettings(
            id=c.id,
            type=c.type,
            name=c.name,
            max_ampere=c.max_ampere,
            enabled=c.enabled
        ),
        ChannelType.ADJ:
        lambda c: AdjustableChannelSettings(
            id=c.id,
            type=c.type,
            name=c.name,
            voltage=c.voltage,
            enabled=c.enabled
        )
    }

    settings_builder = settings_builders[channel.type]

    return settings_builder(channel)


def _create_channel_from_settings_data(settings_data):
    settings_builders = {
        ChannelType.HEATER: lambda data: HeaterChannelSettings(**data),
        ChannelType.POWER: lambda data: PowerChannelSettings(**data),
        ChannelType.ADJ: lambda data: AdjustableChannelSettings(**data),
    }

    channel_type = ChannelType(settings_data['type'])
    settings_builder = settings_builders[channel_type]

    settings = settings_builder(settings_data)

    channel_id = ChannelIdentifier(settings.id)
    descriptor = _get_channel_descriptor(channel_id)

    return _create_channel(descriptor, settings)


def _create_channel(descriptor, settings):
    channel_builders = {
        ChannelType.HEATER:
        lambda d, s: HeaterChannel(
            s.id, s.name, d.in_pin, d.adc_channel, d.den_pin, d.dsel_pin, d.
            dsel_state, s.max_ampere, s.power, s.enabled
        ),
        ChannelType.POWER:
        lambda d, s: PowerChannel(
            s.id, s.name, d.in_pin, d.adc_channel, d.den_pin, d.dsel_pin, d.
            dsel_state, s.max_ampere, s.enabled
        ),
        ChannelType.ADJ:
        lambda d, s:
        AdjustableChannel(s.id, s.name, d.in_pin, s.voltage, s.enabled)
    }

    channel_type = ChannelType(settings.type)
    channel_id = ChannelIdentifier(settings.id)

    descriptor = _get_channel_descriptor(channel_id)

    channel_builder = channel_builders[channel_type]
    channel = channel_builder(descriptor, settings)

    return channel


def _get_default_channel_settings(channel_id):
    default_settings = {
        ChannelIdentifier.HEATER_1:
        HeaterChannelSettings(
            id=channel_id,
            type=ChannelType.HEATER,
            name="Heater Channel 1",
            max_ampere=3.0,
            power=50,
            enabled=False
        ),
        ChannelIdentifier.HEATER_2:
        HeaterChannelSettings(
            id=channel_id,
            type=ChannelType.HEATER,
            name="Heater Channel 2",
            max_ampere=3.0,
            power=50,
            enabled=False
        ),
        ChannelIdentifier.POWER_1:
        PowerChannelSettings(
            id=channel_id,
            type=ChannelType.POWER,
            name="Power Channel 1",
            max_ampere=3.0,
            enabled=False
        ),
        ChannelIdentifier.POWER_2:
        PowerChannelSettings(
            id=channel_id,
            type=ChannelType.POWER,
            name="Power Channel 2",
            max_ampere=3.0,
            enabled=False
        ),
        ChannelIdentifier.POWER_3:
        PowerChannelSettings(
            id=channel_id,
            type=ChannelType.POWER,
            name="Power Channel 3",
            max_ampere=3.0,
            enabled=False
        ),
        ChannelIdentifier.POWER_4:
        PowerChannelSettings(
            id=channel_id,
            type=ChannelType.POWER,
            name="Power Channel 4",
            max_ampere=3.0,
            enabled=False
        ),
        ChannelIdentifier.ADJ_1:
        AdjustableChannelSettings(
            id=channel_id,
            type=ChannelType.ADJ,
            name="Adjustable Channel 1",
            voltage=ChannelVoltage._12V,
            enabled=False
        )
    }

    return default_settings[channel_id]


def _get_channel_descriptor(channel_id):
    descriptor_builders = {
        ChannelIdentifier.HEATER_1:
        ChannelDescriptor(
            in_pin=constants.HEATER_1_IN_PIN,
            adc_channel=constants.HEATER_ADC_CHANNEL,
            den_pin=constants.HEATERS_DEN_PIN,
            dsel_pin=constants.HEATERS_DSEL_PIN,
            dsel_state=constants.HEATER_1_DSEL_STATE
        ),
        ChannelIdentifier.HEATER_2:
        ChannelDescriptor(
            in_pin=constants.HEATER_2_IN_PIN,
            adc_channel=constants.HEATER_ADC_CHANNEL,
            den_pin=constants.HEATERS_DEN_PIN,
            dsel_pin=constants.HEATERS_DSEL_PIN,
            dsel_state=constants.HEATER_2_DSEL_STATE
        ),
        ChannelIdentifier.POWER_1:
        ChannelDescriptor(
            in_pin=constants.POWER_1_IN_PIN,
            adc_channel=constants.POWER_PAIR_1_ADC_CHANNEL,
            den_pin=constants.POWER_PAIR_1_DEN_PIN,
            dsel_pin=constants.POWER_PAIR_1_DSEL_PIN,
            dsel_state=constants.POWER_1_DSEL_STATE
        ),
        ChannelIdentifier.POWER_2:
        ChannelDescriptor(
            in_pin=constants.POWER_2_IN_PIN,
            adc_channel=constants.POWER_PAIR_1_ADC_CHANNEL,
            den_pin=constants.POWER_PAIR_1_DEN_PIN,
            dsel_pin=constants.POWER_PAIR_1_DSEL_PIN,
            dsel_state=constants.POWER_2_DSEL_STATE
        ),
        ChannelIdentifier.POWER_3:
        ChannelDescriptor(
            in_pin=constants.POWER_3_IN_PIN,
            adc_channel=constants.POWER_PAIR_2_ADC_CHANNEL,
            den_pin=constants.POWER_PAIR_2_DEN_PIN,
            dsel_pin=constants.POWER_PAIR_2_DSEL_PIN,
            dsel_state=constants.POWER_3_DSEL_STATE
        ),
        ChannelIdentifier.POWER_4:
        ChannelDescriptor(
            in_pin=constants.POWER_4_IN_PIN,
            adc_channel=constants.POWER_PAIR_2_ADC_CHANNEL,
            den_pin=constants.POWER_PAIR_2_DEN_PIN,
            dsel_pin=constants.POWER_PAIR_2_DSEL_PIN,
            dsel_state=constants.POWER_4_DSEL_STATE
        ),
        ChannelIdentifier.ADJ_1:
        ChannelDescriptor(in_pin=constants.ADJ_IN_PIN)
    }

    return descriptor_builders[channel_id]
