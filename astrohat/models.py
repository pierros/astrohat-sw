from typing import NamedTuple, Dict

from enum import Enum


class ChannelType(str, Enum):
    HEATER = "heater"
    POWER = "power"
    ADJ = "adj"


class ChannelResponseCode:
    SUCCESS = 0
    UNKNOWN_CHANNEL_ID = 1
    UNKNOWN_CHANNEL_TYPE = 2
    INVALID_VALUE = 3
    INVALID_CHANNEL_TYPE = 4


class HeaterChannelStatus(NamedTuple):
    id: str
    type: str
    name: str
    voltage: str
    enabled: bool
    power: int
    ampere: float
    max_ampere: float


class PowerChannelStatus(NamedTuple):
    id: str
    type: str
    name: str
    voltage: str
    enabled: bool
    ampere: float
    max_ampere: float


class AdjustableChannelStatus(NamedTuple):
    id: str
    type: str
    name: str
    voltage: str
    enabled: bool


class ChannelStatus(NamedTuple):
    code: ChannelResponseCode
    channels: Dict[str, NamedTuple]
