from tinyrpc import RPCClient
from tinyrpc.protocols.jsonrpc import JSONRPCProtocol

from .transport import UnixStreamClientTransport
from .models import (
    ChannelType, ChannelResponseCode, ChannelStatus, HeaterChannelStatus,
    PowerChannelStatus, AdjustableChannelStatus
)


class AstrohatClient(object):
    def __init__(self, address):
        self._address = address

        self._transport = None
        self._rpc_client = None

    def connect(self):
        protocol = JSONRPCProtocol()

        self._transport = UnixStreamClientTransport(self._address)
        self._rpc_client = RPCClient(protocol, self._transport)

    def close(self):
        if self._transport:
            self._transport.close()

    def __enter__(self):
        self.connect()

        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def set_channel(self, channel_id, enabled):
        proxy = self._rpc_client.get_proxy()
        response = proxy.set_channel(channel_id, enabled)

        if response is not ChannelResponseCode.SUCCESS:
            error = AstrohatClient._get_error(response)

            return False, error

        return True, None

    def toggle_channel(self, channel_id):
        proxy = self._rpc_client.get_proxy()
        response = proxy.toggle_channel(channel_id)

        if response is not ChannelResponseCode.SUCCESS:
            error = AstrohatClient._get_error(response)

            return False, error

        return True, None

    def channel_name(self, channel_id, name):
        proxy = self._rpc_client.get_proxy()
        response = proxy.channel_name(channel_id, name)

        if response is not ChannelResponseCode.SUCCESS:
            error = AstrohatClient._get_error(response)

            return False, error

        return True, None

    def channel_max_ampere(self, channel_id, max_ampere):
        proxy = self._rpc_client.get_proxy()
        response = proxy.channel_max_ampere(channel_id, max_ampere)

        if response is not ChannelResponseCode.SUCCESS:
            error = AstrohatClient._get_error(response)

            return False, error

        return True, None

    def heater_power(self, channel_id, power):
        proxy = self._rpc_client.get_proxy()
        response = proxy.heater_power(channel_id, power)

        if response is not ChannelResponseCode.SUCCESS:
            error = AstrohatClient._get_error(response)

            return False, error

        return True, None

    def channel_voltage(self, channel_id, voltage):
        proxy = self._rpc_client.get_proxy()
        response = proxy.channel_voltage(channel_id, voltage)

        if response is not ChannelResponseCode.SUCCESS:
            error = AstrohatClient._get_error(response)

            return False, error

        return True, None

    def channel_status(self, channel_id=None, channel_type=None):
        proxy = self._rpc_client.get_proxy()
        response = proxy.channel_status(channel_id, channel_type)

        channel_status = ChannelStatus(*response)

        if channel_status.code is not ChannelResponseCode.SUCCESS:
            return False, []

        def create_channel_status(channel_data):
            status_builder = {
                ChannelType.HEATER: lambda d: HeaterChannelStatus(*d),
                ChannelType.POWER: lambda d: PowerChannelStatus(*d),
                ChannelType.ADJ: lambda d: AdjustableChannelStatus(*d)
            }

            channel_type = ChannelType(channel_data[1])
            builder = status_builder[channel_type]

            return builder(channel_data)

        statuses = [create_channel_status(c) for c in channel_status.channels]

        return True, statuses

    @staticmethod
    def _get_error(code):
        errors = {
            ChannelResponseCode.UNKNOWN_CHANNEL_ID:
            "Unknown channel id",
            ChannelResponseCode.UNKNOWN_CHANNEL_TYPE:
            "Unknown channel type",
            ChannelResponseCode.INVALID_VALUE:
            "Invalid value specified",
            ChannelResponseCode.INVALID_CHANNEL_TYPE:
            "Channel does not support this action"
        }

        error = errors.get(code, f"Unexpected code={code}")

        return f"Unable to execute command: {error}"
