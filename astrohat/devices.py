import spidev
import logging


class MCP3004:
    def __init__(self, bus, device, voltage):
        self._bus = bus
        self._device = device
        self._voltage = voltage

        self._spi = None
        self._open = False

    def read_channel(self, channel, num_samples=1):
        if not self._open:
            max_speed_hz = 200_000 if self._voltage >= 5.0 else 75_000

            logging.debug(
                "Opening SPI interface "
                f"(bus={self._bus}, device={self._device} "
                f"max_speed_hz={max_speed_hz})"
            )

            self._spi = spidev.SpiDev()
            self._spi.open(self._bus, self._device)

            self._spi.max_speed_hz = max_speed_hz

            self._open = True

        adc_aggregate = 0

        for _ in range(num_samples):
            response = self._spi.xfer([1, (8 + channel) << 4, 0])
            value = ((response[1] & 3) << 8) + response[2]

            adc_aggregate += value

        adc_value = adc_aggregate / num_samples

        return adc_value / 1023
