import time
import logging

from enum import Enum
from abc import ABC, abstractmethod

import RPi.GPIO as GPIO

from . import constants
from .devices import MCP3004

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

adc = MCP3004(
    constants.ADC_SPI_BUS, constants.ADC_SPI_DEVICE, constants.ADC_VOLTAGE
)


class ChannelIdentifier(str, Enum):
    HEATER_1 = "heater-1"
    HEATER_2 = "heater-2"
    POWER_1 = "power-1"
    POWER_2 = "power-2"
    POWER_3 = "power-3"
    POWER_4 = "power-4"
    ADJ_1 = "adj-1"


class ChannelType(str, Enum):
    HEATER = "heater"
    POWER = "power"
    ADJ = "adj"

    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_


class ChannelVoltage(str, Enum):
    _3_3V = "3.3V"
    _5V = "5V"
    _8V = "8V"
    _9V = "9V"
    _12V = "12V"

    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_


class Channel(ABC):
    @abstractmethod
    def __init__(self, id, type, name):
        self._id = id
        self._type = type

        self.name = name

    @property
    def id(self):
        return self._id

    @property
    def type(self):
        return self._type

    @property
    def voltage(self):
        return ChannelVoltage._12V

    @abstractmethod
    def loop(self):
        pass

    @abstractmethod
    def shutdown(self):
        pass


class SwitchableChannel(Channel):
    @abstractmethod
    def __init__(self, id, type, name, in_pin, enabled=False):
        super().__init__(id, type, name)

        GPIO.setup(in_pin, GPIO.OUT)

        self._in_pin = in_pin
        self.enabled = enabled

    def toggle(self):
        self.enabled = not self.enabled

    @property
    def enabled(self):
        return bool(GPIO.input(self._in_pin))

    @enabled.setter
    def enabled(self, value):
        GPIO.output(self._in_pin, GPIO.HIGH if value else GPIO.LOW)

    def shutdown(self):
        GPIO.output(self._in_pin, GPIO.LOW)


class MonitoredChannel(SwitchableChannel):
    _ampere = 0.0

    @abstractmethod
    def __init__(
        self,
        id,
        type,
        name,
        in_pin,
        adc_channel,
        den_pin,
        dsel_pin,
        dsel_state,
        max_ampere=constants.MAX_AMPERE,
        enabled=False,
    ):
        super().__init__(id, type, name, in_pin, enabled)

        GPIO.setup(den_pin, GPIO.OUT)
        GPIO.setup(dsel_pin, GPIO.OUT)

        self._adc_channel = adc_channel
        self._den_pin = den_pin
        self._dsel_pin = dsel_pin
        self._dsel_state = dsel_state

        self.max_ampere = max_ampere

    def loop(self):
        # We're only going to monitor the channel while its enabled.
        self._ampere = self._get_ampere() if self.enabled else 0.0

        if self._ampere > self.max_ampere:
            logging.warning(
                "Shutting down channel as exceeded maximum ampere "
                f"(ampere={self._ampere}, max_ampere={self.max_ampere})"
            )

            self.enabled = False

    @property
    def ampere(self):
        return self._ampere

    def _get_ampere(self):
        # Enable diagnostics so we can read the current
        # consumption from the ADC.
        GPIO.output(self._den_pin, GPIO.HIGH)
        GPIO.output(
            self._dsel_pin, GPIO.HIGH if self._dsel_state else GPIO.LOW
        )

        # We get a value between 0 and 1 which is relative
        # to the max adc voltage.
        adc_value = adc.read_channel(self._adc_channel, constants.ADC_SAMPLES)

        # Disable diagnostics for this channel.
        GPIO.output(self._den_pin, GPIO.LOW)
        GPIO.output(self._dsel_pin, GPIO.LOW)

        voltage = constants.ADC_VOLTAGE * adc_value
        ampere = (voltage * constants.KILLIS) / 1000.0

        return ampere

    def shutdown(self):
        super().shutdown()

        GPIO.output(self._den_pin, GPIO.LOW)
        GPIO.output(self._dsel_pin, GPIO.LOW)


class HeaterChannel(MonitoredChannel):
    def __init__(
        self,
        id,
        name,
        in_pin,
        adc_channel,
        den_pin,
        dsel_pin,
        dsel_state,
        max_ampere=constants.MAX_AMPERE,
        power=constants.DEFAULT_HEATER_POWER,
        enabled=False,
    ):
        super().__init__(
            id, ChannelType.HEATER, name, in_pin, adc_channel, den_pin,
            dsel_pin, dsel_state, max_ampere, enabled
        )

        self._enabled = enabled

        self.power = power

    def loop(self):
        super().loop()

        if not self._enabled:
            return

        active_period = constants.HEATER_FREQUENCY - (
            constants.HEATER_FREQUENCY * (self.power / 100.0)
        )

        start = (time.time() * 1000.0) % constants.HEATER_FREQUENCY
        enabled = start >= active_period or self.power >= 100

        # super does not support fset so it requires manual invocation.
        super(MonitoredChannel, self.__class__).enabled.fset(self, enabled)

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, value):
        self._enabled = value


class PowerChannel(MonitoredChannel):
    def __init__(
        self,
        id,
        name,
        in_pin,
        adc_channel,
        den_pin,
        dsel_pin,
        dsel_state,
        max_ampere=constants.MAX_AMPERE,
        enabled=False,
    ):
        super().__init__(
            id, ChannelType.POWER, name, in_pin, adc_channel, den_pin,
            dsel_pin, dsel_state, max_ampere, enabled
        )

    def loop(self):
        super().loop()


class AdjustableChannel(SwitchableChannel):
    def __init__(
        self, id, name, in_pin, voltage=ChannelVoltage._12V, enabled=False
    ):
        super().__init__(id, ChannelType.ADJ, name, in_pin, enabled)

        self._voltage = voltage

    def loop(self):
        super().loop()

    @property
    def voltage(self):
        return self._voltage

    @voltage.setter
    def voltage(self, value):
        self._voltage = value
