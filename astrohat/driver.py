import time
import logging

from . import constants
from .settings import load_or_create_channels


class AstrohatDriver:
    def __init__(self):
        self._running = False
        self._channels = None

    @property
    def running(self):
        return self._running

    def start(self):
        if not self._running:
            logging.info("Starting Astrohat driver")

            self._running = True

            self._run()

    def stop(self):
        logging.info("Stopping Astrohat driver")

        self._running = False

    def _run(self):
        while True:
            if not self._running:
                break

            for channel in self.channels:
                channel.loop()

            time.sleep(constants.DRIVER_CYCLE_DELAY)

        logging.info("Astrohat driver stopping")
        for channel in self.channels:
            channel.shutdown()

        logging.info("Astrohat driver has stopped")

    @property
    def channels(self):
        if not self._channels:
            self._channels = load_or_create_channels()

        return self._channels

    def channel(self, id):
        return next((c for c in self.channels if c.id == id), None)


driver = AstrohatDriver()
