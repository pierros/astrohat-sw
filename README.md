# Astrohat
Astrohat is a Raspberry Pi 4 compatible hat which can power and monitor all
your astronomy equipment.

This repository contains the daemon and command-line interface for direct
control.

# Installation

## Pre-requisites
Debian/Ubuntu:

```
sudo apt-get install -y python3.6 make upx-ucl
```

## Cloning Repository
```
git clone https://gitlab.com/pierros/astrohat-sw && \
cd astrohat-sw
```

## Rootless Installation
Astrohat is installed in the user space and therefore does not require root. It does
however require some configuration to get up and running.

You can start by building the binaries and installing them into the user home with
the following commands.
```
make && make install
```

Once installed, the Astrohat binaries location will need to be added to your `PATH`.
Along with some required environment variables. Use a text editor of your choice to update your `.bashrc`.
```
vi ~/.bashrc
```

Add the following:
```
# Astrohat Configuration
export ASTROHAT_HOST=$HOME/.astrohat/run/astrohat.sock
export PATH=$PATH:$HOME/.astrohat/bin
```

Once saved, remember to reload your `.bashrc`.
```
source ~/.bashrc
```

# Astrohat CLI
To list available commands, either run `astrohat` with no parameters or execute
`astrohat --help`

```
Usage: astrohat [OPTIONS] COMMAND [ARGS]...

  Smart power for all your astronomy equipment.

Options:
  --debug  Run in debug mode.
  --help   Show this message and exit.

Commands:
  channel  Control and retrieve channels.
```

## Channels
```
Usage: astrohat channel [OPTIONS] COMMAND [ARGS]...

  Control and retrieve channels.

Options:
  --help  Show this message and exit.

Commands:
  disable     Disable a channel.
  enable      Enable a channel.
  max-ampere  Set the maximum ampere a channel can consume.
  name        Update the name of a channel.
  power       Update the power setting of a heater channel.
  set         Set the on/off state of a channel.
  status      Get the status of one or more channels.
  toggle      Toggle a channel on or off.
  voltage     Change the output voltage of a channel.
```

# Development
You can create the development virtual environment using the following command

```
make setup
```

## Running the Tests
Execute the following command to run the test suite.

```
make test
```

## Running the daemon
After setup has complete, you can start the daemon in the terminal process using
the following command

```
make run
```

## Installing dependencies
Any additional dependencies can be added in the `requirements/*.in` files.
For example, a development only dependency can be added in the
`requirements/development.in` but if a dependency is required in both it can be
added to the `requirements/release.in` file.

Once added, the dependency requirements files `release/development.txt` will
require re-compiling. Use the following command to compile these files. And update
the python environment.

```
make update
```

**_NOTE:_** Ensure any changes to the compiled requirement files are pushed to source
control.

# License
Distributed under the GPLV3 License. See `LICENSE` for more information.
