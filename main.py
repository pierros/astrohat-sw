import os
import sys
import click
import logging
import signal

from logging.handlers import TimedRotatingFileHandler

from daemon import DaemonContext
from daemon.pidfile import TimeoutPIDLockFile

from astrohat.constants import HEADER
from astrohat.server import AstrohatServer

ASTROHAT_HOST = os.environ.get("ASTROHAT_HOST", "astrohat.sock")
ASTROHAT_LOG = os.environ.get("ASTROHAT_LOG", "astrohat.log")
ASTROHAT_PID = os.environ.get("ASTROHAT_PID", "astrohat.pid")


@click.command()
@click.option(
    "-f", "--foreground", is_flag=True, help="Run as a foreground process."
)
@click.option("--debug", is_flag=True, help="Run in debug mode.")
def cli(foreground=False, debug=False):
    logging.basicConfig(
        level=logging.DEBUG if debug else logging.INFO,
        format="%(asctime)s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"
    )

    context = DaemonContext(
        pidfile=TimeoutPIDLockFile(ASTROHAT_PID, -1),
        working_directory=os.getcwd()
    )

    if not foreground:
        # Attach the logging output to syslog when running in the background
        log_file_handle = TimedRotatingFileHandler(
            ASTROHAT_LOG, when="D", interval=1, backupCount=2
        )

        # Add the log file handle to the root logger.
        root = logging.getLogger()
        root.addHandler(log_file_handle)

        # Ensure we preserve the file handle when forking.
        context.files_preserve = [log_file_handle.stream]

        context.open()
    else:
        print(HEADER)

    if debug:
        logging.debug("Astrohat daemon running in DEBUG mode")

    try:
        server = AstrohatServer(ASTROHAT_HOST)

        def exit_gracefully(signum, frame):
            logging.debug(
                "Astrohat gracefully exiting "
                f"(signum={signum}, frame={frame})"
            )

            server.stop()
            context.close()

            sys.exit(0)

        signal.signal(signal.SIGINT, exit_gracefully)
        signal.signal(signal.SIGTERM, exit_gracefully)

        server.start()

        exit_gracefully()
    except Exception as ex:
        logging.error(f"An unhandled error occured: {ex}")


if __name__ == "__main__":
    cli()
