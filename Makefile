# Makefile

# Installation variables
ASTROHAT_BUILD := ./build
ASTROHAT_DIST := ./dist
ASTROHAT_INSTALL = $(HOME)/.astrohat

# Systemd variables
SYSTEMD_UNIT := astrohat.service
SYSTEMD_CONFIG := $(HOME)/.config/systemd/user/

.PHONY: default
__default__:
	@echo "Building virtual python environment"
	pip3 install virtualenv
	python3 -m venv $(ASTROHAT_BUILD)/env
	$(ASTROHAT_BUILD)/env/bin/pip3 install -r requirements/release.txt

	$(ASTROHAT_BUILD)/env/bin/pip3 install pyinstaller

	@echo "Building Astrohat binaries"

	$(ASTROHAT_BUILD)/env/bin/pyinstaller \
		--workpath $(ASTROHAT_BUILD) \
		--specpath $(ASTROHAT_BUILD) \
		--distpath $(ASTROHAT_DIST) \
		--onefile \
		--name astrohatd \
		main.py

	$(ASTROHAT_BUILD)/env/bin/pyinstaller \
		--workpath $(ASTROHAT_BUILD) \
		--specpath $(ASTROHAT_BUILD) \
		--distpath $(ASTROHAT_DIST) \
		--onefile \
		--name astrohat \
		cli.py

install:
	@echo "Installing Astrohat $(ASTROHAT_PATH)"

	mkdir -p $(ASTROHAT_INSTALL)/bin
	mkdir -p $(ASTROHAT_INSTALL)/run
	mkdir -p $(ASTROHAT_INSTALL)/log

	cp -r dist/astrohat $(ASTROHAT_INSTALL)/bin/astrohat
	cp -r dist/astrohatd $(ASTROHAT_INSTALL)/bin/astrohatd
	cp -r LICENSE $(ASTROHAT_INSTALL)/LICENSE

	@echo "Creating $(SYSTEMD_CONFIG)/$(SYSTEMD_UNIT)"
	mkdir -p $(SYSTEMD_CONFIG)
	cp $(SYSTEMD_UNIT) $(SYSTEMD_CONFIG)

	systemctl --user daemon-reload
	systemctl --user enable $(SYSTEMD_UNIT)
	systemctl --user start $(SYSTEMD_UNIT)
	@echo "Installed $(SYSTEMD_UNIT) successfully."

.PHONY: uninstall
uninstall:
	@echo "Uninstalling Astrohat $(ASTROHAT_INSTALL)"

	systemctl --user stop $(SYSTEMD_UNIT)
	systemctl --user disable $(SYSTEMD_UNIT)
	rm -f $(SYSTEMD_CONFIG)/$(SYSTEMD_UNIT)
	systemctl --user daemon-reload
	@echo "Uninstalled $(SYSTEMD_UNIT)"

	rm -f -r $(ASTROHAT_INSTALL)

	@echo Please remove any Astrohat configuration from your .bashrc

.PHONY: setup
setup:
	pip3 install virtualenv
	python3 -m venv env

	env/bin/pip3 install pip-tools
	env/bin/pip-sync requirements/development.txt

.PHONY: update
update:
	env/bin/pip-compile requirements/release.in
	env/bin/pip-compile requirements/development.in
	env/bin/pip-sync requirements/development.txt

.PHONY: run
run:
	env/bin/python3 main.py -f

.PHONY: test
test:
	env/bin/python3 -m pytest tests

.PHONY: clean
clean:
	find . -type d -name __pycache__ -exec rm -r -f {} \+
	find . -type d -name .pytest_cache -exec rm -r -f {} \+

	rm -f -r env
	rm -f settings.json
	rm -f astrohat.sock
	rm -f astrohat.pid
	rm -f astrohat.log
	rm -f -r $(ASTROHAT_BUILD)
	rm -f -r $(ASTROHAT_DIST)
